﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
   public class CUENTA
    {
        #region "atributos"
        private string cliente_nombre;
        private double cliente_balance;
        private string cliente_cuenta;
        private string tipo_cuenta;
        #endregion

        #region "propiedades"
        public string Nombre
        {
            get { return cliente_nombre; }
            set { cliente_nombre = value; }
        }
        public double Balance
        {
            get { return cliente_balance; }
        }
        public string Nro_Cta
        {
            get { return cliente_cuenta; }
            set { cliente_cuenta = value; }
        }
        public string Tipo_Cta
        {
            get { return tipo_cuenta; }
            set { tipo_cuenta = value; }
        }
        #endregion

        #region "Métodos"
        public void deposito(double cantidad)
        {
            cliente_balance += cantidad;
        }

        public bool retiro(double cantidad)
        {
            if (cliente_balance >= cantidad)
            {
                cliente_balance -= cantidad;
                return true;
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Insuficientes fondos" + "\n" + "Operacón Invalida");
                return false;
            }
        }
        #endregion
    }
}

