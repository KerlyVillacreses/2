﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _2
{
    public partial class Form1 : Form
    {
        CUENTA cuenta = new CUENTA();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cuenta.Nro_Cta = textBox1.Text;
            cuenta.Nombre = textBox2.Text;
            cuenta.Tipo_Cta = textBox3.Text;
            if (cuenta.Nro_Cta == "")
            {
                groupBox1.Enabled = true;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double cantidad = double.Parse(textBox4.Text);
            cuenta.deposito(cantidad);
            listBox1.Items.Add("El número de cuenta bancaria es: " + cuenta.Nro_Cta + ", MONTO DEPOSITADO: " + cantidad + " Bs");

        }

        private void button4_Click(object sender, EventArgs e)
        {
            double cantidad = double.Parse(textBox5.Text);
            if (cuenta.retiro(cantidad))
            {
                listBox1.Items.Add("El número de cuenta bancaria es: " + cuenta.Nro_Cta + ", MONTO RETIRADO: " + cantidad + " Bs");
            }
            else
            {
                listBox1.Items.Add("El número de cuenta bancaria es: " + cuenta.Nro_Cta + "--/ OPERACIÓN NO VÁLIDA (SALDO INSUFICIENTE) /--");
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("E S T A D O   D E   C U E N T A" + "\n" + "Número de cuenta: " + cuenta.Nro_Cta + "\n" + "Nombre del Cliente: " + cuenta.Nombre + "\n" + "Tipo de Cuenta: " + cuenta.Tipo_Cta + "\n" + "\n" + "SALDO DE LA CUENTA: " + cuenta.Balance.ToString() + "\n" + "\n" + DateTime.Now.ToString());
        }
    }
}